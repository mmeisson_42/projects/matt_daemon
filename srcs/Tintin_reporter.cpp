
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "Tintin_reporter.hpp"

using namespace Log;


/*
**	Constructors
*/

Tintin_reporter::Tintin_reporter(void) :
	_cout_write( false ),
	_current_level( INFO ),
	_keep_alive( true ),
	_log_file_name( NULL ),
	_log_file( NULL )
{

}

Tintin_reporter::Tintin_reporter( const Tintin_reporter & inst ) :
	_cout_write( inst.get_cout_write() ),
	_current_level( inst.get_level() ),
	_keep_alive( inst.get_keep_alive() ),
	_log_file_name( NULL )
{
	if (inst.get_log_file_name() != NULL)
	{
		_log_file_name = new string(*inst.get_log_file_name());
	}
}

Tintin_reporter::~Tintin_reporter(void)
{
	if (_log_file.is_open())
	{
		_log_file.close();
	}
}

Tintin_reporter	&	Tintin_reporter::operator=(const Tintin_reporter & inst)
{
	_cout_write = inst.get_cout_write();
	_current_level = inst.get_level();
	_keep_alive = inst.get_keep_alive();
	delete _log_file_name;
	if (inst.get_log_file_name() != NULL)
	{
		_log_file_name = new string(*inst.get_log_file_name());
	}
	else
	{
		_log_file_name = NULL;
	}
	return *this;
}

/*
**	Utils
*/

string			Tintin_reporter::level_to_string(Level level)
{
	switch (level)
	{
		case DEBUG:
			return string("debug");
		case INFO:
			return string("info");
		case WARNING:
			return string("warning");
		case ERROR:
			return string("error");
		case FATAL:
			return string("fatal");
		default:
			return string();
	}
}



/*
**	Loggers
*/

inline void		_log(ostream & stream, const string & message, Level level)
{
	time_t		now = time(NULL);
	struct tm	*timeinfo;
	char		buffer[1024];

	timeinfo = localtime(&now);
	strftime(buffer, 1024, "[%d/%m/%Y-%H:%M:%S]", timeinfo);
	stream << buffer << " [" << Tintin_reporter::level_to_string(level) << "] " << message  << std::endl;
}

void			Tintin_reporter::log_level(const std::string & message, Level level)
{
	if (level >= _current_level)
	{
		if (_cout_write == true)
		{
			_log(cout, message, level);
		}
		if (_log_file_name != NULL)
		{
			if (_log_file.is_open() == false)
			{
				_log_file.open(_log_file_name->c_str(), std::ofstream::out | std::ofstream::app);
				if (_log_file.fail())
				{
					cerr << "[WARNING] :: Could not open log file " << _log_file_name << endl;
					return ;
				}
			}

			_log(_log_file, message, level);
			if (_keep_alive == false)
			{
				_log_file.close();
			}
		}
	}
}

void		Tintin_reporter::log_debug(const std::string & message) { this->log_level(message, DEBUG); }
void		Tintin_reporter::log_info(const std::string & message) { this->log_level(message, INFO); }
void		Tintin_reporter::log_warning(const std::string & message) { this->log_level(message, WARNING); }
void		Tintin_reporter::log_error(const std::string & message) { this->log_level(message, ERROR); }
void		Tintin_reporter::log_fatal(const std::string & message) { this->log_level(message, FATAL); }


/*
**	Getters / Setters
*/

void			Tintin_reporter::set_level(Level level)
{
	_current_level = level;
}

void			Tintin_reporter::set_log_file_name(const string & file_name)
{
	if (_log_file_name != NULL)
	{
		delete _log_file_name;
	}

	{
		/* Create the base directory if needed */
		size_t	last_slash = file_name.find_last_of("\\/");
		if (last_slash < file_name.length())
		{
			string file_path = file_name.substr(0, last_slash);
			mkdir(file_path.c_str(), 0755);
		}
	}
	_log_file_name = new string(file_name);
}

Level			Tintin_reporter::get_level(void) const { return _current_level; }
bool			Tintin_reporter::get_cout_write(void) const { return _cout_write; }
bool			Tintin_reporter::get_keep_alive(void) const { return _keep_alive; }
const string	*Tintin_reporter::get_log_file_name(void) const { return _log_file_name; }
