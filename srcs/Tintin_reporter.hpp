#ifndef TINTIN_REPORTER_H
# define TINTIN_REPORTER_H

# include <fstream>
# include <string>

using namespace std;

namespace Log
{
	enum Level
	{
		DEBUG,
		INFO,
		WARNING,
		ERROR,
		FATAL
	};

	class Tintin_reporter
	{
		public:

			Tintin_reporter(void);
			Tintin_reporter( const Tintin_reporter & inst);
			~Tintin_reporter(void);

			static string		level_to_string(Level level);

			void		log_level(const string & message, Level level);

			void		log_debug(const string & message);
			void		log_info(const string & message);
			void		log_warning(const string & message);
			void		log_error(const string & message);
			void		log_fatal(const string & message);

			void		set_level(Level level);
			void		set_log_file_name(const string & level);
			void		keep_alive(void);
			void		not_keep_alive(void);

			Level			get_level(void) const;
			const string	*get_log_file_name(void) const;
			bool			get_cout_write(void) const;
			bool			get_keep_alive(void) const;

		private:
			bool		_cout_write;
			Level		_current_level;

			bool		_keep_alive;
			string		*_log_file_name;
			ofstream	_log_file;

			Tintin_reporter	& operator=(const Tintin_reporter & inst);
	};
}

#endif
