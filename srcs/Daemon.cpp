
#include <iostream>
#include <stdexcept>
#include <string>
#include <cstring>
#include <cerrno>
#include <cstdlib>
#include <fcntl.h>
#include <unistd.h>
#include <sys/file.h>

#include "Daemon.hpp"

Daemon				*Daemon::_instance = NULL;

const Daemon		*Daemon::get_instance(const string & lock_name)
{
	if (Daemon::_instance == NULL)
	{
		Daemon::_instance = new Daemon(lock_name);
	}
	return Daemon::_instance;
}

const Daemon		*Daemon::get_instance(void)
{
	if (Daemon::_instance == NULL)
	{
		throw runtime_error("Daemon::get_instance(void) called before singleton instanciation via Daemon::get_instance(lock_name)");
	}
	return Daemon::_instance;
}

void				Daemon::remove_instance(void)
{
	if (Daemon::_instance != NULL)
	{
		delete Daemon::_instance;
		Daemon::_instance = NULL;
	}
}

void				Daemon::remove_instance(int signal)
{
	(void)signal;
	delete Daemon::_instance;
	cerr << "Going to die";
	exit(0);
}

Daemon::Daemon(const string & lock_name) :
	_lock_name( lock_name )
{
	const string		lock_path = string("/var/lock/") + _lock_name;

	_lock_fd = open(lock_path.c_str(), O_CREAT);

	if (_lock_fd == -1)
	{
		throw runtime_error(strerror(errno));
	}

	if (flock(_lock_fd, LOCK_EX | LOCK_NB) == -1)
	{
		throw runtime_error(strerror(errno));
	}
}

Daemon::~Daemon(void)
{
	const string		lock_path = string("/var/lock/") + _lock_name;

	unlink(lock_path.c_str());
}
