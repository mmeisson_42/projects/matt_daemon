#ifndef DAEMON_CPP
# define DAEMON_CPP

# include "Tintin_reporter.hpp"

using namespace Log;

class Daemon
{
	public:
		~Daemon(void);

		static const Daemon		*get_instance(void);
		static const Daemon		*get_instance(const string & lock_name);
		static void				remove_instance(void);
		static void				remove_instance(int signal);

	private:
		Daemon(const string & lock_name);

		static Daemon		*_instance;
		string				_lock_name;
		int					_lock_fd;
};

#endif
