#ifndef SERVER_TEXT_HPP
# define SERVER_TEXT_HPP

# include <string>
# include <vector>
# include <functional>

using namespace std;

class Server_text
{
	public:
		Server_text(void);
		Server_text( const Server_text & instance );
		Server_text(unsigned short port);
		~Server_text(void);

		Server_text & operator=(const Server_text & instance );

		int					get_socket(void) const;
		const vector<int>	&get_clients(void) const;
		const vector<sockaddr_in*>	&get_clients_sockaddr(void) const;
		unsigned short		get_port(void) const;
		void				set_port(unsigned short set_port);

		int					connect(void);
		void				disconnect(void);
		void				poll(function<int (const string &, const struct sockaddr_in &)>);

	private:
		unsigned short		_port;
		int					_socket_fd;
		vector<int>			_clients;
		vector<sockaddr_in*>		_clients_sockaddr;

	int					_call_callback(int fd, function<int (const string &, const struct sockaddr_in &)>callback);
};

#endif
