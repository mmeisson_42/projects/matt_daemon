
#include <vector>
#include <algorithm>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "Server_text.hpp"

using namespace std;

Server_text::Server_text(void) :
	_port( 4242 ),
	_socket_fd( -1 )
{

}

Server_text::Server_text( const Server_text & inst ) :
	_port( inst.get_port() ),
	_socket_fd( inst.get_socket() )
{
	for (auto client: inst.get_clients())
	{
		_clients.push_back(dup(client));
	}
	for (auto client_sockaddr: inst.get_clients_sockaddr())
	{
		_clients_sockaddr.push_back(new sockaddr_in(*client_sockaddr));
	}
}

Server_text::Server_text( unsigned short port ) :
	_port( port ),
	_socket_fd( -1 )
{

}

Server_text::~Server_text(void)
{
	this->disconnect();
}

Server_text		&	Server_text::operator=( const Server_text & inst )
{
	_clients_sockaddr.clear();
	_clients.clear();

	_port = inst.get_port();
	_socket_fd = inst.get_socket();
	for (auto client: inst.get_clients())
	{
		_clients.push_back(dup(client));
	}
	for (auto client_sockaddr: inst.get_clients_sockaddr())
	{
		_clients_sockaddr.push_back(new sockaddr_in(*client_sockaddr));
	}
	return *this;
}

unsigned short	Server_text::get_port( void ) const { return _port; }
int				Server_text::get_socket( void ) const { return _socket_fd; }

const vector<int>	&			Server_text::get_clients( void ) const { return _clients; }
const vector<sockaddr_in*>	&	Server_text::get_clients_sockaddr( void ) const { return _clients_sockaddr; }

void			Server_text::set_port( unsigned short port )
{
	_port = port;
}

int			Server_text::connect( void )
{
	sockaddr_in		sin = {
		.sin_family = AF_INET,
		.sin_port = htons(_port),
		.sin_addr = { .s_addr = INADDR_ANY },
		.sin_zero = { 0 },
	};
	protoent		*p = getprotobyname("tcp");

	_socket_fd = socket(AF_INET, SOCK_STREAM, p->p_proto);
	if (bind(_socket_fd, reinterpret_cast<sockaddr *>(&sin), sizeof sin) < 0)
	{
		close(_socket_fd);
		_socket_fd = -1;
		return -1;
	}

	if (listen(_socket_fd, 3) < 0)
	{
		close(_socket_fd);
		_socket_fd = -1;
		return -1;
	}
	return 0;
}

void		Server_text::disconnect( void )
{
	for (auto client: _clients)
	{
		close(client);
	}
	_clients.clear();

	if (_socket_fd != -1)
	{
		close(_socket_fd);
		_socket_fd = -1;
	}
}

int		Server_text::_call_callback(int fd, function<int (const string &, const struct sockaddr_in &)> callback)
{
	sockaddr_in	*sin = NULL;
	char		buffer[256];
	int			client_index;

	auto found_iter = find(_clients.begin(), _clients.end(), fd);

	if (found_iter != _clients.end())
	{
		client_index = distance(_clients.begin(), found_iter);
		sin = _clients_sockaddr[client_index];

		size_t	bytes_read = read(fd, buffer, 255);

		if (bytes_read == 0)
		{
			close(fd);
			_clients.erase(_clients.begin() + client_index);
			_clients_sockaddr.erase(_clients_sockaddr.begin() + client_index);
		}
		else
		{
			buffer[bytes_read] = 0;
			return callback(string(buffer), *sin);
		}
	}
	return 0;
}

void		Server_text::poll(function<int (const string &, const struct sockaddr_in &)> callback)
{
	bool			should_stop = 0;
	fd_set			set;
	int				max_fd;
	sockaddr_in		*connecting = NULL;
	socklen_t		connecting_size;
	int				connecting_fd;

	while (should_stop == false)
	{
		FD_ZERO(&set);

		FD_SET(_socket_fd, &set);
		max_fd = _socket_fd;
		for (auto client: _clients)
		{
			if (client > max_fd)
				max_fd = client;
			FD_SET(client, &set);
		}
		select(max_fd + 1, &set, NULL, NULL, NULL);
		for (auto client: _clients)
		{
			if (FD_ISSET(client, &set))
			{
				if (_call_callback(client, callback) < 0)
				{
					should_stop = true;
				}
			}
		}
		if (FD_ISSET(_socket_fd, &set))
		{
			connecting = new sockaddr_in;
			connecting_size = sizeof(sockaddr_in);
			connecting_fd = accept(_socket_fd, reinterpret_cast<sockaddr *>(connecting), &connecting_size);

			if (connecting_fd >= 0 && _clients.size() < 3)
			{
				_clients.push_back(connecting_fd);
				_clients_sockaddr.push_back(connecting);
			}
			else
			{
				if (connecting_fd >= 0)
					close(connecting_fd);
				delete connecting;
			}
		}
	}
}
