
#include <fstream>
#include <csignal>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <cerrno>
#include <string>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Tintin_reporter.hpp"
#include "Server_text.hpp"
#include "Daemon.hpp"

using namespace Log;
using namespace std;

Server_text			server;
Tintin_reporter			logger;


static void		init_daemon(Tintin_reporter &logger)
{
	try
	{
		Daemon::get_instance("matt_daemon.lock");
	}
	catch (runtime_error &e)
	{
		logger.log_error("- Matt_daemon :: file /var/lock/matt_daemon.lock could not be opened");
		exit(EXIT_FAILURE);
	}
}

static void		double_fork(Tintin_reporter &logger)
{
	pid_t		pid;

	pid = fork();
	switch(pid)
	{
		case -1:
			logger.log_fatal("- Matt_daemon :: Could not enter daemon mode - abort");
			exit(1);
			break;
		case 0:
			usleep(5000);
			close(0); close(1); close(2);
			setsid();
			chdir("/");
			pid = fork();
			switch(pid)
			{
				case -1:
					logger.log_fatal("- Matt_daemon :: Could not enter daemon mode - abort");
					exit(1);
					break ;
				case 0:
					close(0); close(1); close(2);
					logger.log_info("- Matt_daemon :: Started");
					break ;
				default:
					logger.log_info( "- Matt_daemon :: started. Pid " + to_string(pid) );
					exit(0);
			}
			break ;
		default:
			exit(0);
	}
}


int main(void)
{

	auto eid = geteuid();

	if (eid > 0)
	{
		cout << "Hey, I don't know how to work with limited privileges. Gimme root access please :)" << std::endl;
		return EXIT_FAILURE;
	}

	logger.set_log_file_name("/var/log/matt_daemon/matt_daemon.log");

	init_daemon(logger);

	logger.log_info("- Matt_daemon :: Entering daemon mode");

	double_fork(logger);

	for (int i = 1; i < 50; i++)
	{
		signal(
			i,
			[] (int) -> void
			{
				logger.log_info( "- Matt_daemon :: going to quit (signaled)" );
				server.disconnect();
				logger.log_info( "- Matt_daemon :: disconnected" );
				Daemon::remove_instance();
				logger.log_info( "- Matt_daemon :: byebye " );
				exit(0);
			}
		);
	}

	if (server.connect() == -1)
	{
		logger.log_error("- Matt_daemon :: Could not start server - abort");
		logger.log_error( strerror(errno) );
		return EXIT_FAILURE;
	}

	server.poll([] (const string & message, const sockaddr_in & sin) -> int {

		string complete_message = string("(") + inet_ntoa(sin.sin_addr) + ") ";

		if (message.length() > 0 && message[message.length() - 1] == '\n')
		{
			complete_message += message.substr(0, message.length() - 1);
		}
		else
		{
			complete_message += message;
		}
		logger.log_info(complete_message);

		if (message.compare(0, 4, "quit") == 0)
		{
			return -1;
		}
		return 0;
	});

	logger.log_info( "- Matt_daemon :: going to quit" );
	server.disconnect();
	logger.log_info( "- Matt_daemon :: disconnected" );
	Daemon::remove_instance();
	logger.log_info( "- Matt_daemon :: byebye " );
	return EXIT_SUCCESS;
}
