
NAME			= Matt_daemon

CC				= c++

CFLAGS			= -MD -Wall -Werror -Wextra --std=c++11

VPATH			= ./srcs/

SRCS			= main.cpp Daemon.cpp Server_text.cpp Tintin_reporter.cpp

INCS_PATHS		= ./srcs
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.cpp=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))


DEPS			= $(OBJS:.o=.d)

LIB_PATHS		=
LIBS			=

LDFLAGS			=



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j32 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.cpp Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

self_clean:
	rm -rf $(OBJS_PATH)

clean: self_clean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) clean;)

self_fclean:
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

fclean: self_fclean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) fclean;)

re:
	make fclean
	make -j32 all

test:
	make -C tests
	./tests/ft_nmap_tests

-include $(DEPS)
